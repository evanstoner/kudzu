# Kudzu

_Kudzu is an IaaS and automation abstraction layer for cyber experiments._


[![pipeline status](https://gitlab.com/evanstoner/kudzu/badges/master/pipeline.svg)](https://gitlab.com/evanstoner/kudzu/commits/master)
[![Documentation Status](https://readthedocs.org/projects/kudzu/badge/?version=latest)](http://kudzu.readthedocs.io/en/latest/?badge=latest)

[https://kudzu.readthedocs.io](https://kudzu.readthedocs.io)

## Running

1. `mvn spring-boot:run`
2. `http://localhost:8080`
