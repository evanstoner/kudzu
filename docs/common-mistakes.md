# Common developer mistakes

**Could not read document: Unresolved forward references for: Object id [1]...**

This error occurs when attempting to deserialize a new record with a reference to another record, but
the reference is only in the form of an ID. You need to tell Jackson that this property is expected
to be an ID, not the object itself. Consider a Build:

```
// <imports and annotations>
public class Build {

    // <other fields and methods>

    @ManyToOne
    @JoinColumn(name = "testbed", updatable = false, nullable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Testbed testbed;

    public Testbed getTestbed() {
        return testbed;
    }

    public void setTestbed(Testbed testbed) {
        this.testbed = testbed;
    }
}

```

This JSON will fail to deserialize because Jackson did not find a Testbed object embedded:

```
{
  "build": {
    "name": "new build",
    "testbed": 1
  }
}
```

To resolve this, add an additional setter to Build that takes a Long, and instruct Jackson to use this
as the JSON property:

```
    @JsonProperty("testbed")
    public void setTestbed(Long testbedId) {
        this.testbed = new Testbed(testbedId);
    }
```

You'll need to ensure Testbed as a public constructor which takes an ID.

**Entity inheritance**

When using entity inheritance with the `TABLE_PER_CLASS` strategy (i.e. table per concrete class),
the entities' identifiers must be totally unique. Consider `Employee` and `Manager`, which
both extend `Person`. `alice` is an instance of `Employee` and `bob` is an instance of `Manager`. It
cannot be the case that `alice` and `bob` both have ID 1, even though this is not restricted from a
database constraint perspective. Given an ID, Hibernate will not know which table to fetch from.

Because of this, using an ID generation strategy of `TABLE` will fail, because it _would_ assign ID's
as described above. Instead, we must use `SEQUENCE` so that all ID's are assigned based on the same
sequence of numbers. This ensures uniqueness of records across the database.

Alternatively, use a String identifier such as UUID.

**Generic entity fields**

Consider a generic class that has a relationship to another entity:

```
public abstract class Instance<D> extends BaseEntity {
    @ManyToOne
    private D base;
}
```

The first problem with this is that we don't restrict the type argument. In this example, Instance
bases should only be Testbed, Host, Network, or Link. So we'll create a shell abstract class:

```
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class TestbedEntity extends BaseEntity {
}
```

The entity inheritance is not required here since the only purpose at this point is to restrict the
type argument to `Instance`, but it may be useful later for us to make queries on the superclass.

Now, we can appropriately restrict the type argument.

```
public abstract class Instance<D extends TestbedEntity> extends BaseEntity {
    @ManyToOne
    private D base;
}
```

Next problem: Hibernate will choose which concrete `TestbedEntity` to map this field to, even on
concrete `Instance`s that don't have the same typed field. For example, with `Instance<Foo>` and
`Instance<Bar>`, Hibernate will type both classes' `base` field as `Foo` (or maybe `Bar`). To work
around this, specify that the target entity of that relationship is the superclass.

```
public abstract class Instance<D extends TestbedEntity> extends BaseEntity {
    @ManyToOne(targetEntity = TestbedEntity.class)
    private D base;
}
```

Now, the field will be of the appropriate type for each concrete `Instance`.

**No qualifying bean of type [net.evanstoner.kudzu.core.service.GenericReadOnlyService] is defined: expected single matching bean but found X: ...**

This error occurs when a service implementation has not been annotated as `@Service`, as we haven't
instructed Spring where the bean to fulfill this service can be found.