package net.evanstoner.kudzu.controller;

import com.jayway.restassured.RestAssured;
import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import com.jayway.restassured.http.ContentType;
import net.evanstoner.kudzu.KudzuApplication;
import net.evanstoner.kudzu.tenancy.domain.Project;
import net.evanstoner.kudzu.tenancy.repository.ProjectRepository;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = KudzuApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class ProjectControllerIntegrationTests {

    @Value("${local.server.port}")
    private int serverPort;

    // -- end boilerplate --

    @Autowired
    ProjectRepository projectRepository;

    private static final String PROJECTS_ROUTE = "/projects";
    private static final String PROJECT_ROUTE = "/projects/{id}";

    private Project projectA;
    private static final String PROJECT_A_NAME = "aaa";

    private Project projectB;
    private static final String PROJECT_B_NAME = "bbb";

    @Before
    public void setup() {
        projectA = projectRepository.save(new Project(PROJECT_A_NAME));
        projectB = projectRepository.save(new Project(PROJECT_B_NAME));

        RestAssured.port = serverPort;
    }

    @Test
    public void canGetAllProjects() {
        when()
                .get(PROJECTS_ROUTE)
        .then()
                .statusCode(HttpStatus.SC_OK)
                .body("projects.name", hasItems(PROJECT_A_NAME, PROJECT_B_NAME));
    }

    @Test
    public void canGetOneProject() {
        given()
                .pathParam("id", projectA.getId())
        .when()
                .get(PROJECT_ROUTE)
        .then()
                .statusCode(HttpStatus.SC_OK)
                .body("project.name", equalTo(PROJECT_A_NAME));
    }

    @Test
    public void nonexistentProjectReturnsNotFound() {
        given()
                .pathParam("id", 99)
        .when()
                .get(PROJECT_ROUTE)
        .then()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test
    public void canCreateProject() {
        Project newProject = new Project("new");
        Map<String, Object> body = new HashMap<>();
        body.put("project", newProject);

        given()
                .contentType(ContentType.JSON)
                .body(body)
        .when()
                .post(PROJECTS_ROUTE)
        .then()
                .statusCode(HttpStatus.SC_OK)
                .body("project.name", equalTo("new"));
    }

    @Test
    public void canUpdateProject() {
        Project updatedProjectA = projectA;
        updatedProjectA.setName("updated");
        Map<String, Object> body = new HashMap<>();
        body.put("project", updatedProjectA);

        given()
                .contentType(ContentType.JSON)
                .body(body)
                .pathParam("id", projectA.getId())
        .when()
                .put(PROJECT_ROUTE)
        .then()
                .statusCode(HttpStatus.SC_OK)
                .body("project.name", equalTo("updated"));
    }

    @Test
    public void canDeleteProject() {
        given()
                .basePath(PROJECT_ROUTE)
                .pathParam("id", projectA.getId())
        .when()
                .delete()
        .then()
                .statusCode(HttpStatus.SC_NO_CONTENT);

        given()
                .basePath(PROJECT_ROUTE)
                .pathParam("id", projectA.getId());

        assertFalse("the project exists in the repository", projectRepository.exists(projectA.getId()));
    }


}
