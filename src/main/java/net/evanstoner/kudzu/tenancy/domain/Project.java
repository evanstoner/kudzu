package net.evanstoner.kudzu.tenancy.domain;

import com.fasterxml.jackson.annotation.JsonRootName;
import net.evanstoner.spring.entity.BaseEntity;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;

@Entity
@JsonRootName("project")
public class Project extends BaseEntity {

    @NotBlank
    private String name;

    private String description;

    Project() {}

    public Project(String name) {
        setName(name);
    }

    public Project(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
