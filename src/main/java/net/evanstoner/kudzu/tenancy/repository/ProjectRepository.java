package net.evanstoner.kudzu.tenancy.repository;

import net.evanstoner.kudzu.tenancy.domain.Project;
import org.springframework.data.repository.CrudRepository;

public interface ProjectRepository extends CrudRepository<Project, Long> {

}
