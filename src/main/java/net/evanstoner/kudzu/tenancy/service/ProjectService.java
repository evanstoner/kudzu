package net.evanstoner.kudzu.tenancy.service;

import net.evanstoner.spring.service.GenericReadWriteService;
import net.evanstoner.kudzu.tenancy.domain.Project;

public interface ProjectService extends GenericReadWriteService<Project> {
}
