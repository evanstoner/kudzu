package net.evanstoner.kudzu.tenancy.service;

import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import net.evanstoner.kudzu.tenancy.domain.Project;
import net.evanstoner.kudzu.tenancy.repository.ProjectRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ProjectServiceImpl extends GenericReadWriteServiceImpl<Project, ProjectRepository> implements ProjectService {
}
