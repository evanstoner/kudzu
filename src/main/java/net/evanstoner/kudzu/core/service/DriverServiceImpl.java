package net.evanstoner.kudzu.core.service;

import net.evanstoner.kudzu.build.driver.RouterDriver;
import net.evanstoner.kudzu.build.driver.TestbedDriver;
import net.evanstoner.kudzu.build.service.BuildService;
import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.provision.driver.ProvisionDriver;
import net.evanstoner.kudzu.provision.driver.SourceDriver;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Constructor;

@Service
public class DriverServiceImpl implements DriverService {

    @Autowired
    private BuildService buildService;

    @Override
    public TestbedDriver loadTestbedDriver(String classname, String config) throws ReflectiveOperationException, ConfigurationException {
        Class<TestbedDriver> clazz = (Class<TestbedDriver>)Class.forName(classname);
        Constructor<TestbedDriver> constructor = clazz.getConstructor(BuildService.class);
        TestbedDriver driver = constructor.newInstance(buildService);
        driver.configure(config);
        return driver;
    }

    @Override
    public RouterDriver loadRouterDriver(String classname, String config) throws ReflectiveOperationException, ConfigurationException {
        Class<RouterDriver> clazz = (Class<RouterDriver>)Class.forName(classname);
        RouterDriver driver = clazz.newInstance();
        driver.configure(config);
        return driver;
    }

    @Override
    public ProvisionDriver loadProvisionDriver(String classname, String config) throws ReflectiveOperationException, ConfigurationException {
        Class<ProvisionDriver> clazz = (Class<ProvisionDriver>)Class.forName(classname);
        ProvisionDriver driver = clazz.newInstance();
        driver.configure(config);
        return driver;
    }

    @Override
    public SourceDriver loadSourceDriver(String classname, String config) throws ReflectiveOperationException, ConfigurationException {
        Class<SourceDriver> clazz = (Class<SourceDriver>)Class.forName(classname);
        SourceDriver driver = clazz.newInstance();
        driver.configure(config);
        return driver;
    }

}
