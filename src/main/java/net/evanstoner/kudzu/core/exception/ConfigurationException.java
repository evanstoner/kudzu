package net.evanstoner.kudzu.core.exception;

public class ConfigurationException extends Exception {

    public ConfigurationException(String reason) {
        super(reason);
    }

    public ConfigurationException(String reason, Exception cause) {
        super(reason, cause);
    }

}
