package net.evanstoner.kudzu.core.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class ValidationExceptionHandler {

    /**
     * Handle validation exceptions (i.e. arguments annotated with @Valid that fail validation)
     * with an appropriately verbose response, instead of the default.
     * @param ex
     * @return
     */
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public @ResponseBody
    HttpEntity<Map> handleValidationException(MethodArgumentNotValidException ex) {
        Map<String, Object> response = new HashMap<>();
        List<ConciseFieldError> conciseErrors = new ArrayList<>();

        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            conciseErrors.add(new ConciseFieldError(fieldError));
        }

        response.put("object", ex.getBindingResult().getObjectName());
        response.put("errors", conciseErrors);
        return new HttpEntity<Map>(response);
    }

}
