package net.evanstoner.kudzu.core.controller;

import org.springframework.validation.FieldError;
public class ConciseFieldError {

    private FieldError fieldError;

    public ConciseFieldError(FieldError fieldError) {
        this.fieldError = fieldError;
    }

    public String getField() {
        return this.fieldError.getField();
    }

    public String getMessage() {
        return this.fieldError.getDefaultMessage();
    }

    public Object getRejectedValue() {
        return this.fieldError.getRejectedValue();
    }

}
