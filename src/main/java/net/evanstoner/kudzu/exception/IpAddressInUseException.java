package net.evanstoner.kudzu.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class IpAddressInUseException extends RuntimeException {

    public IpAddressInUseException(String ipAddress) {
        super(ipAddress + " is already in use on the specified network");
    }

}
