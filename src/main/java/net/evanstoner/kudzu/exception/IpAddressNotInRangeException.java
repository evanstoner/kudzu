package net.evanstoner.kudzu.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class IpAddressNotInRangeException extends RuntimeException {

    public IpAddressNotInRangeException(String ip, String cidr) {
        super(ip + " is not in the range of addresses for " + cidr);
    }

}
