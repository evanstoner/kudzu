package net.evanstoner.kudzu.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidCidrException extends RuntimeException {

    public InvalidCidrException(String cidr) {
        super(cidr + " is not a valid CIDR");
    }

}
