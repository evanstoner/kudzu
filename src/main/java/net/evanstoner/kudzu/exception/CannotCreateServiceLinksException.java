package net.evanstoner.kudzu.exception;

public class CannotCreateServiceLinksException extends RuntimeException {

    public CannotCreateServiceLinksException() {
        super("links on the service network cannot be created directly " +
                "(they are created automatically with the host)");
    }

}
