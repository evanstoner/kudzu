package net.evanstoner.kudzu.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidSshPortException extends RuntimeException {

    public InvalidSshPortException(Integer port) {
        super(port + " is not a valid SSH port; must be between 1024 and 65535");
    }

}
