package net.evanstoner.kudzu.provision.repository;

import net.evanstoner.kudzu.provision.domain.PersonaAssignment;
import net.evanstoner.kudzu.testbed.domain.Host;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PersonaAssignmentRepository extends CrudRepository<PersonaAssignment, Long> {

    @Query("SELECT pa FROM PersonaAssignment pa JOIN pa.host h WHERE h.testbed = ?1")
    Iterable<PersonaAssignment> findByHostTestbed(Testbed testbed);

}
