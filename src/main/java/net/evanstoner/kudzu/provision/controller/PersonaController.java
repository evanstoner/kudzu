package net.evanstoner.kudzu.provision.controller;

import net.evanstoner.spring.controller.GenericReadWriteController;
import net.evanstoner.kudzu.provision.domain.Persona;
import net.evanstoner.kudzu.provision.service.PersonaService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/personas")
public class PersonaController extends GenericReadWriteController<Persona, PersonaService> {
}
