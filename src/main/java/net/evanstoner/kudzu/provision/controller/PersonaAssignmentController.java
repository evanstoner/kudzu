package net.evanstoner.kudzu.provision.controller;

import net.evanstoner.spring.controller.GenericReadWriteController;
import net.evanstoner.kudzu.provision.domain.PersonaAssignment;
import net.evanstoner.kudzu.provision.service.PersonaAssignmentService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/personaAssignments")
public class PersonaAssignmentController extends GenericReadWriteController<PersonaAssignment, PersonaAssignmentService> {

    @RequestMapping(method = RequestMethod.GET, params = "testbed")
    public Map getByTestbedId(@RequestParam("testbed") Long testbedId) {
        return wrapMany(this.service.findByTestbedId(testbedId));
    }

}
