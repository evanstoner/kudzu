package net.evanstoner.kudzu.provision.exception;

import net.evanstoner.kudzu.provision.driver.ProvisionDriver;

public class UnsupportedFeatureException extends ProvisionException {

    public UnsupportedFeatureException(ProvisionDriver.ProvisionFeature feature) {
        super("This driver does not support feature: " + feature);
    }

}
