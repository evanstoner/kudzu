package net.evanstoner.kudzu.provision.exception;

public class ProvisionException extends Exception {
    public ProvisionException(String message) {
        super(message);
    }
}
