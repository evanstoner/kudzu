package net.evanstoner.kudzu.provision.domain;

import com.fasterxml.jackson.annotation.JsonRootName;
import net.evanstoner.spring.entity.BaseEntity;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;

@Entity
@JsonRootName("provisioner")
public class Provisioner extends BaseEntity {

    @NotBlank
    private String name;

    @NotBlank
    private String provisionDriver;

    private String provisionConfig;

    public Provisioner() {}

    public Provisioner(Long id) {
        super(id);
    }

    public String getProvisionConfig() {
        return provisionConfig;
    }

    public void setProvisionConfig(String provisionConfig) {
        this.provisionConfig = provisionConfig;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvisionDriver() {
        return provisionDriver;
    }

    public void setProvisionDriver(String provisionDriver) {
        this.provisionDriver = provisionDriver;
    }

    @Override
    public String toString() {
        return super.toString() + "[" + provisionDriver + "]";
    }

}
