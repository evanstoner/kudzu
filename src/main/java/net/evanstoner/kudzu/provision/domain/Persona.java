package net.evanstoner.kudzu.provision.domain;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import net.evanstoner.spring.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@JsonRootName("persona")
public class Persona extends BaseEntity {

    private String name;

    private String source;

    private String sourceDriver;

    private String defaultConfig;

    @ManyToOne
    @JoinColumn(name = "provisioner", nullable = false, updatable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Provisioner provisioner;

    public Persona() {}

    public Persona(Long id) {
        super(id);
    }

    public String getDefaultConfig() {
        return defaultConfig;
    }

    public void setDefaultConfig(String defaultConfig) {
        this.defaultConfig = defaultConfig;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Provisioner getProvisioner() {
        return provisioner;
    }

    public void setProvisioner(Provisioner provisioner) {
        this.provisioner = provisioner;
    }

    @JsonProperty("provisioner")
    public void setProvisioner(Long provisionerId) {
        this.provisioner = new Provisioner(provisionerId);
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceDriver() {
        return sourceDriver;
    }

    public void setSourceDriver(String sourceDriver) {
        this.sourceDriver = sourceDriver;
    }
}
