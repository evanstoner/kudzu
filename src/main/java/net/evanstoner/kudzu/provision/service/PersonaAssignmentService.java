package net.evanstoner.kudzu.provision.service;

import net.evanstoner.spring.service.GenericReadWriteService;
import net.evanstoner.kudzu.provision.domain.PersonaAssignment;

public interface PersonaAssignmentService extends GenericReadWriteService<PersonaAssignment> {

    Iterable<PersonaAssignment> findByTestbedId(Long testbedId);

}
