package net.evanstoner.kudzu.provision.service;

import net.evanstoner.spring.service.GenericReadWriteService;
import net.evanstoner.kudzu.provision.domain.Provisioner;

public interface ProvisionerService extends GenericReadWriteService<Provisioner> {
}
