package net.evanstoner.kudzu.provision.service;

import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import net.evanstoner.kudzu.provision.domain.Provisioner;
import net.evanstoner.kudzu.provision.repository.ProvisionerRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class ProvisionerServiceImpl
        extends GenericReadWriteServiceImpl<Provisioner, ProvisionerRepository>
        implements ProvisionerService {
}
