package net.evanstoner.kudzu.provision.service;

import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import net.evanstoner.kudzu.provision.domain.PersonaAssignment;
import net.evanstoner.kudzu.provision.repository.PersonaAssignmentRepository;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class PersonaAssignmentServiceImpl
        extends GenericReadWriteServiceImpl<PersonaAssignment, PersonaAssignmentRepository>
        implements PersonaAssignmentService {

    @Override
    public Iterable<PersonaAssignment> findByTestbedId(Long testbedId) {
        return this.repository.findByHostTestbed(new Testbed(testbedId));
    }

}
