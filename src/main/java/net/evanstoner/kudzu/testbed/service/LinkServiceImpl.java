package net.evanstoner.kudzu.testbed.service;

import net.evanstoner.kudzu.exception.CannotCreateServiceLinksException;
import net.evanstoner.kudzu.exception.InvalidIpAddressException;
import net.evanstoner.kudzu.exception.IpAddressInUseException;
import net.evanstoner.kudzu.exception.IpAddressNotInRangeException;
import net.evanstoner.kudzu.testbed.domain.Host;
import net.evanstoner.kudzu.testbed.domain.Link;
import net.evanstoner.kudzu.testbed.domain.Network;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import net.evanstoner.kudzu.testbed.repository.LinkRepository;
import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import org.apache.commons.net.util.SubnetUtils;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.ThreadLocalRandom;

@Transactional
@Service
public class LinkServiceImpl extends GenericReadWriteServiceImpl<Link, LinkRepository> implements LinkService {

    @Autowired
    NetworkService networkService;

    @Override
    public Iterable<Link> findByTestbedId(Long testbedId) {
        return this.repository.findByTestbed(new Testbed(testbedId));
    }

    @Override
    public Link create(Link link) {
        if (link.getNetwork().getCidr() == null) {
            // the network reference is empty and only contains the id (i.e. it was created from
            // a JSON reference like `network: 21` but was not actually queried; do that now to fill
            // out its attributes
            link.setNetwork(networkService.findOne(link.getNetwork().getId()));
        }

        // service links can only be created using createServiceLinkForHost, not directly
        if (link.getNetwork().isService()) {
            throw new CannotCreateServiceLinksException();
        }

        if (link.getIpAddress() == null || link.getIpAddress().length() == 0) {
            link.setIpAddress(nextIpAddressForNetwork(link.getNetwork()));
        } else {
            // verify the ip address is valid
            if (!InetAddressValidator.getInstance().isValid(link.getIpAddress())) {
                throw new InvalidIpAddressException(link.getIpAddress());
            }

            // verify the ip address is in the correct range
            SubnetUtils subnet = new SubnetUtils(link.getNetwork().getCidr());
            if (!subnet.getInfo().isInRange(link.getIpAddress())) {
                throw new IpAddressNotInRangeException(link.getIpAddress(), link.getNetwork().getCidr());
            }

            // verify the ip address is not in use
            if (repository.countByNetworkAndIpAddress(link.getNetwork(), link.getIpAddress()) > 0) {
                throw new IpAddressInUseException(link.getIpAddress());
            }
        }
        return save(link);
    }

    @Override
    public Link createServiceLinkForHost(Host host) {
        Testbed testbed = host.getTestbed();
        Network serviceNetwork = networkService.findServiceNetworkForTestbed(testbed);
        Link link = new Link();
        link.setHost(host);
        link.setNetwork(serviceNetwork);
        link.setTestbed(testbed);
        link.setIpAddress(nextIpAddressForNetwork(serviceNetwork));
        return save(link);
    }

    private String nextIpAddressForNetwork(Network network) {
        SubnetUtils subnet = new SubnetUtils(network.getCidr());
        //TODO: assign the NEXT sequentially available IP (maybe)
        //FIXME: check availablility of the address before returning it
        int addressIndex = ThreadLocalRandom.current().nextInt(10, 249);
        return subnet.getInfo().getAllAddresses()[addressIndex];
    }
}
