package net.evanstoner.kudzu.testbed.service;

import net.evanstoner.kudzu.build.driver.RouterDriver;
import net.evanstoner.kudzu.build.service.BuildService;
import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.core.service.DriverService;
import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import net.evanstoner.kudzu.provider.domain.Provider;
import net.evanstoner.kudzu.provider.service.ProviderService;
import net.evanstoner.kudzu.testbed.domain.Router;
import net.evanstoner.kudzu.testbed.domain.RouterType;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import net.evanstoner.kudzu.testbed.repository.RouterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class RouterServiceImpl extends GenericReadWriteServiceImpl<Router, RouterRepository> implements RouterService {

    private static final Logger LOG = LoggerFactory.getLogger(RouterServiceImpl.class);

    @Autowired
    DriverService driverService;

    @Autowired
    ProviderService providerService;

    @Override
    public Router createRouterForTestbed(Testbed testbed) {
        Router router = new Router();
        RouterType routerType = null;
        Provider provider = providerService.findOne(testbed.getProvider().getId());
        try {
            RouterDriver routerDriver = driverService.loadRouterDriver(provider.getRouterDriver(), provider.getRouterConfig());
            routerType = routerDriver.getType();
        } catch (ReflectiveOperationException | ConfigurationException e) {
            LOG.error("couldn't load router driver to determine type", e);
        }
        router.setTestbed(testbed);
        router.setType(routerType);
        return repository.save(router);
    }

    @Override
    public Router findByTestbedId(Long testbedId) {
        return repository.findByTestbed(new Testbed(testbedId));
    }

}
