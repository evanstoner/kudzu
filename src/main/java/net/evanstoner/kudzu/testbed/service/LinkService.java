package net.evanstoner.kudzu.testbed.service;

import net.evanstoner.kudzu.testbed.domain.Host;
import net.evanstoner.kudzu.testbed.domain.Link;
import net.evanstoner.spring.service.GenericReadWriteService;

public interface LinkService extends GenericReadWriteService<Link> {

    public Iterable<Link> findByTestbedId(Long testbedId);

    public Link createServiceLinkForHost(Host host);

}
