package net.evanstoner.kudzu.testbed.service;

import net.evanstoner.kudzu.testbed.domain.Host;
import net.evanstoner.spring.service.GenericReadWriteService;

public interface HostService extends GenericReadWriteService<Host> {

    public Iterable<Host> findByTestbedId(Long testbedId);

}
