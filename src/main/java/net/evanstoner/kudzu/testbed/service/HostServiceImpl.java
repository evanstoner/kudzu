package net.evanstoner.kudzu.testbed.service;

import net.evanstoner.kudzu.testbed.domain.Host;
import net.evanstoner.kudzu.testbed.domain.Link;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import net.evanstoner.kudzu.testbed.repository.HostRepository;
import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Transactional
@Service
public class HostServiceImpl extends GenericReadWriteServiceImpl<Host, HostRepository> implements HostService {

    @Autowired
    LinkService linkService;

    @Override
    public Iterable<Host> findByTestbedId(Long testbedId) {
        return this.repository.findByTestbed(new Testbed(testbedId));
    }

    @Override
    public Host create(Host host) {
        Host savedHost = save(host);
        Link link = this.linkService.createServiceLinkForHost(savedHost);
        savedHost.setLinks(Collections.singleton(link));
        return savedHost;
    }

}
