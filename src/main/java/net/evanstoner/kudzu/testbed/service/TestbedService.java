package net.evanstoner.kudzu.testbed.service;

import net.evanstoner.kudzu.testbed.domain.Testbed;
import net.evanstoner.spring.service.GenericReadWriteService;

public interface TestbedService extends GenericReadWriteService<Testbed> {

}
