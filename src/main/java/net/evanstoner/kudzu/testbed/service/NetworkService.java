package net.evanstoner.kudzu.testbed.service;

import net.evanstoner.kudzu.testbed.domain.Network;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import net.evanstoner.spring.service.GenericReadWriteService;

public interface NetworkService extends GenericReadWriteService<Network> {

    public Iterable<Network> findByTestbedId(Long testbedId);

    public Network findServiceNetworkForTestbed(Testbed testbed);

    public Network createServiceNetworkForTestbed(Testbed testbed);

}
