package net.evanstoner.kudzu.testbed.service;

import net.evanstoner.kudzu.testbed.domain.Network;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import net.evanstoner.kudzu.testbed.repository.NetworkRepository;
import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class NetworkServiceImpl extends GenericReadWriteServiceImpl<Network, NetworkRepository> implements NetworkService {

    @Override
    public Iterable<Network> findByTestbedId(Long testbedId) {
        return this.repository.findByTestbed(new Testbed(testbedId));
    }

    @Override
    public Network findServiceNetworkForTestbed(Testbed testbed) {
        return this.repository.findOneByTestbedAndService(testbed, true);
    }

    @Override
    public Network createServiceNetworkForTestbed(Testbed testbed) {
        Network network = new Network();
        network.setName("service");
        network.setCidr(nextServiceCidr());
        network.setTestbed(testbed);
        network.setService(true);
        return this.repository.save(network);
    }

    private String nextServiceCidr() {
        //FIXME: generate a unique CIDR for this testbed
        return "10.0.1.0/24";
    }

}
