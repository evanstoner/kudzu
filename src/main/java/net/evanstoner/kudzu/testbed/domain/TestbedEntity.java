package net.evanstoner.kudzu.testbed.domain;

import net.evanstoner.spring.entity.BaseEntity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class TestbedEntity extends BaseEntity {

}
