package net.evanstoner.kudzu.testbed.domain;

public enum RouterType {

    /**
     * The router exposes its own external IP, and supports the creation of port forwarding rules that
     * map external requests to the service network. The router does not perform IP forwarding; it
     * only rewrites packets' destination addresses. Whether or not the router performs NAT is
     * inconsequential.
     *
     * Attributes: external IP
     * Input: service address, service port
     * Output: external port
     */
    PORT_FORWARD,

    /**
     * The router exposes unique public IPs for each host instance. The router may or may not support
     * IP forwarding to service addresses.
     *
     * Attributes: none
     * Input: service address
     * Output: public IP
     */
    PUBLIC_IP,

    /**
     * The router acts as a gateway to service addresses, and as such exposes its own public IP.
     * The instances are accessible at their service addresses as long as the client configures
     * an associated route.
     *
     * Attributes: external IP
     * Input: none
     * Output: none
     */
    GATEWAY,

    /**
     * The router acts as a VPN server for the service network. Once connected, a client may reach
     * host instances on their service addresses.
     *
     * Attributes: external IP, VPN protocol, VPN username, VPN password, VPN data (key/value pairs)
     * Input: none
     * Output: none
     */
    VPN,

    /**
     * There is no router; the host instances are directly accessible at their service network
     * addresses.
     *
     * Attributes: none
     * Input: none
     * Output: none
     */
    FLAT

}
