package net.evanstoner.kudzu.testbed.domain;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
@JsonRootName("link")
public class Link extends TestbedEntity {

    private String ipAddress;

    @ManyToOne
    @JoinColumn(name = "host", nullable = false, updatable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Host host;

    @ManyToOne
    @JoinColumn(name = "network", nullable = false, updatable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Network network;

    @ManyToOne
    @JoinColumn(name = "testbed", nullable = false, updatable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Testbed testbed;

    public Link() {}

    public Link(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    @JsonProperty("host")
    public void setHost(Long hostId) {
        this.host = new Host(hostId);
    }

    public Network getNetwork() {
        return network;
    }

    public void setNetwork(Network network) {
        this.network = network;
    }

    @JsonProperty("network")
    public void setNetwork(Long networkId) {
        this.network = new Network(networkId);
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public void setTestbed(Testbed testbed) {
        this.testbed = testbed;
    }

    @JsonProperty("testbed")
    public void setTestbed(Long testbedId) {
        this.testbed = new Testbed(testbedId);
    }
}
