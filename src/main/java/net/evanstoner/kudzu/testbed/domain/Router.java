package net.evanstoner.kudzu.testbed.domain;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonRootName;

import javax.persistence.*;

@Entity
@JsonRootName("router")
public class Router extends TestbedEntity {

    @Enumerated(value = EnumType.STRING)
    private RouterType type;

    @OneToOne
    @JoinColumn(name = "testbed", updatable = false, nullable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Testbed testbed;

    public RouterType getType() {
        return type;
    }

    public void setType(RouterType type) {
        this.type = type;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public void setTestbed(Testbed testbed) {
        this.testbed = testbed;
    }
}
