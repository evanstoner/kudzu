package net.evanstoner.kudzu.testbed.domain;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

@Entity
@JsonRootName("network")
public class Network extends TestbedEntity {

    @NotBlank
    private String name;

    @NotBlank
    private String cidr;

    @Column(updatable = false)
    private boolean service = false;

    @ManyToOne
    @JoinColumn(name = "testbed", updatable = false, nullable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Testbed testbed;

    public Network() {}

    public Network(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCidr() {
        return cidr;
    }

    public void setCidr(String cidr) {
        this.cidr = cidr;
    }

    public boolean isService() {
        return service;
    }

    public void setService(boolean service) {
        this.service = service;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public void setTestbed(Testbed testbed) {
        this.testbed = testbed;
    }

    @JsonProperty("testbed")
    public void setTestbed(Long testbedId) {
        this.testbed = new Testbed(testbedId);
    }
}
