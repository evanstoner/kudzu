package net.evanstoner.kudzu.testbed.domain;

import com.fasterxml.jackson.annotation.*;
import net.evanstoner.kudzu.provider.domain.Flavor;
import net.evanstoner.kudzu.provider.domain.Image;
import net.evanstoner.kudzu.provision.domain.PersonaAssignment;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@JsonRootName("host")
public class Host extends TestbedEntity {

    @NotBlank
    private String name;

    @ElementCollection
    private List<Integer> accessiblePorts = new ArrayList<>();

    @ManyToOne
    //TODO:  should image be updateable = false?
    @JoinColumn(name = "image", nullable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Image image;

    @ManyToOne
    //TODO:  should flavor be updateable = false?
    @JoinColumn(name = "flavor", nullable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Flavor flavor;

    @ManyToOne
    @JoinColumn(name = "testbed", updatable = false, nullable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Testbed testbed;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "host")
    @Cascade(CascadeType.DELETE)
    @JsonIdentityReference(alwaysAsId = true)
    private Collection<Link> links;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "host")
    @Cascade(CascadeType.DELETE)
    @JsonIdentityReference(alwaysAsId = true)
    private Collection<PersonaAssignment> personaAssignments;

    Host() {}

    public Host(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getAccessiblePorts() {
        return accessiblePorts;
    }

    public void setAccessiblePorts(List<Integer> accessiblePorts) {
        this.accessiblePorts = accessiblePorts;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @JsonProperty("image")
    public void setImage(Long imageId) {
        this.image = new Image(imageId);
    }

    public Flavor getFlavor() {
        return flavor;
    }

    public void setFlavor(Flavor flavor) {
        this.flavor = flavor;
    }

    @JsonProperty("flavor")
    public void setFlavor(Long flavorId) {
        this.flavor = new Flavor(flavorId);
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public void setTestbed(Testbed testbed) {
        this.testbed = testbed;
    }

    @JsonProperty("testbed")
    public void setTestbed(Long testbedId) {
        this.testbed = new Testbed(testbedId);
    }

    public Collection<Link> getLinks() {
        return links;
    }

    @JsonIgnore
    public Link getServiceLink() {
        if (links == null || links.size() == 0) return null;

        return links.stream()
                .filter(l -> l.getNetwork().isService())
                .findFirst().get();
    }

    public void setLinks(Collection<Link> links) {
        this.links = links;
    }

    public Collection<PersonaAssignment> getPersonaAssignments() {
        return personaAssignments;
    }

    public void setPersonaAssignments(Collection<PersonaAssignment> personaAssignments) {
        this.personaAssignments = personaAssignments;
    }
}
