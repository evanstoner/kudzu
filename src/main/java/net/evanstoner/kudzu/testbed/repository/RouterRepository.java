package net.evanstoner.kudzu.testbed.repository;

import net.evanstoner.kudzu.testbed.domain.Router;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import org.springframework.data.repository.CrudRepository;

public interface RouterRepository extends CrudRepository<Router, Long> {

    Router findByTestbed(Testbed testbed);

}
