package net.evanstoner.kudzu.testbed.repository;

import net.evanstoner.kudzu.testbed.domain.Testbed;
import org.springframework.data.repository.CrudRepository;

public interface TestbedRepository extends CrudRepository<Testbed, Long> {

    Iterable<Testbed> findByProject(Long projectId);

}
