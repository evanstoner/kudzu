package net.evanstoner.kudzu.testbed.repository;

import net.evanstoner.kudzu.testbed.domain.Link;
import net.evanstoner.kudzu.testbed.domain.Network;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import org.springframework.data.repository.CrudRepository;

public interface LinkRepository extends CrudRepository<Link, Long> {

    public Iterable<Link> findByTestbed(Testbed testbed);

    public int countByNetworkAndIpAddress(Network network, String ipAddress);

}
