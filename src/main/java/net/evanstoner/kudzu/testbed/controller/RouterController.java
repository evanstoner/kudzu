package net.evanstoner.kudzu.testbed.controller;

import net.evanstoner.spring.controller.GenericReadOnlyController;
import net.evanstoner.kudzu.testbed.domain.Router;
import net.evanstoner.kudzu.testbed.service.RouterService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/routers")
public class RouterController extends GenericReadOnlyController<Router, RouterService> {
}
