package net.evanstoner.kudzu.testbed.controller;

import net.evanstoner.spring.controller.GenericReadWriteController;
import net.evanstoner.kudzu.testbed.domain.Network;
import net.evanstoner.kudzu.testbed.service.NetworkService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/networks")
public class NetworkController extends GenericReadWriteController<Network, NetworkService> {

    @RequestMapping(method = RequestMethod.GET, params = "testbed")
    public Map getByTestbedId(@RequestParam("testbed") Long testbedId) {
        return wrapMany(this.service.findByTestbedId(testbedId));
    }

}
