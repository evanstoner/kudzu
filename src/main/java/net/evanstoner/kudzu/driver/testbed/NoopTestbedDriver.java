package net.evanstoner.kudzu.driver.testbed;

import net.evanstoner.kudzu.build.domain.*;
import net.evanstoner.kudzu.build.driver.TestbedDriver;
import net.evanstoner.kudzu.build.service.BuildService;
import net.evanstoner.kudzu.testbed.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class NoopTestbedDriver extends TestbedDriver {

    private static final Logger LOG = LoggerFactory.getLogger(NoopTestbedDriver.class);

    public NoopTestbedDriver(BuildService buildService) {
        super(buildService);
    }

    public void configure(String config) {
    }

    @Override
    public String buildTestbed(Testbed testbed) {
        return nextId();
    }

    @Override
    public void destroyTestbedInstance(TestbedInstance testbedInstance) {

    }

    @Override
    public String buildNetwork(Network network) {
        return nextId();
    }

    @Override
    public void destroyNetworkInstance(NetworkInstance networkInstance) {

    }

    @Override
    public String buildHost(Host host) {
        return nextId();
    }

    @Override
    public void destroyHostInstance(HostInstance hostInstance) {

    }

    @Override
    public String buildLink(Link link) {
        return nextId();
    }

    @Override
    public void destroyLinkInstance(LinkInstance linkInstance) {

    }

    @Override
    public String buildRouter(Router router, TestbedInstance testbedInstance) {
        return nextId();
    }

    @Override
    public void destroyRouterInstance(RouterInstance routerInstance) {

    }

    private String nextId() {
        return UUID.randomUUID().toString();
    }

}
