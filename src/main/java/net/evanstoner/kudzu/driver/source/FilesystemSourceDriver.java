package net.evanstoner.kudzu.driver.source;

import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.provision.driver.SourceDriver;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FilesystemSourceDriver extends SourceDriver {

    @Override
    public void configure(String config) throws ConfigurationException {

    }

    public void retrieve(String source) throws IOException {
        if (!Files.exists(Paths.get(source))) {
            throw new FileNotFoundException(source);
        }
    }

}
