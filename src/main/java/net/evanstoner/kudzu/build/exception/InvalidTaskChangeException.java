package net.evanstoner.kudzu.build.exception;

import net.evanstoner.kudzu.build.domain.BuildTask;

public class InvalidTaskChangeException extends Exception {

    public InvalidTaskChangeException(BuildTask task) {
        this(task, null);
    }

    public InvalidTaskChangeException(BuildTask task, String reason) {
        super("Cannot set build's task to " + task
                + (reason != null ? ": " + reason : ""));
    }

}
