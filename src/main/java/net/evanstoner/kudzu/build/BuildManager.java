package net.evanstoner.kudzu.build;

import net.evanstoner.kudzu.build.domain.BuildState;
import net.evanstoner.kudzu.build.domain.BuildTask;
import net.evanstoner.kudzu.build.driver.RouterDriver;
import net.evanstoner.kudzu.build.driver.TestbedDriver;
import net.evanstoner.kudzu.build.exception.InvalidStateChangeException;
import net.evanstoner.kudzu.build.exception.InvalidTaskChangeException;
import net.evanstoner.kudzu.build.scheduler.ImmediateScheduler;
import net.evanstoner.kudzu.build.scheduler.Scheduler;
import net.evanstoner.kudzu.build.domain.Build;
import net.evanstoner.kudzu.build.service.BuildService;
import net.evanstoner.kudzu.build.service.InstanceService;
import net.evanstoner.kudzu.build.task.DestroyBuildTask;
import net.evanstoner.kudzu.build.task.InstantiateBuildTask;
import net.evanstoner.kudzu.build.task.ProvisionBuildTask;
import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.core.service.DriverService;
import net.evanstoner.kudzu.provider.domain.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class BuildManager {

    private static final Logger LOG = LoggerFactory.getLogger(BuildManager.class);

    @Autowired
    BuildService buildService;

    @Autowired
    DriverService driverService;

    @Autowired
    InstanceService instanceService;

    //FIXME: load the scheduler based on config
    private Scheduler scheduler = new ImmediateScheduler();

    //FIXME: load the number of threads in the build pool from config
    private ExecutorService executorService = Executors.newFixedThreadPool(1);

    public BuildManager() {
        //TODO: find STARTED builds and set them to ERROR (since we found them at startup)
    }

    /**
     * Submit the Build so that it will be properly scheduled.
     * @param build
     */
    public void submit(Build build) {
        build.setSubmittedOn(new Date());
        build.setStartsOn(scheduler.schedule(build.getTestbed()));
        build.setState(BuildState.SCHEDULED);
        LOG.info("scheduled " + build + " for " + build.getStartsOn());
    }

    /**
     * Notify that the indicated task on the indicated build has begun.
     * @param build
     * @param task
     */
    public Build notifyBuildTaskStarted(Build build, BuildTask task) throws InvalidTaskChangeException {
        return buildService.setBuildTask(build, task);
    }

    /**
     * Notify that the indicated task on the indicated build has completed successfully.
     * @param build
     * @param task
     */
    public Build notifyBuildTaskEnded(Build build, BuildTask task) throws InvalidTaskChangeException {
        Build savedBuild = buildService.setBuildTask(build, null);

        switch (task) {
            case BUILDING:
                // when the build is done being built, start provisioning it
                provisionBuild(savedBuild);
                break;
            case PROVISIONING:
                // when the build is done being provisioned, it's considered active
                try {
                    buildService.notifyActive(savedBuild);
                } catch (InvalidStateChangeException e) {
                    LOG.error("failed to set " + build + " active");
                }
                break;
            case DESTROYING:
                try {
                    buildService.notifyDestroyed(savedBuild);
                } catch (InvalidStateChangeException e) {
                    LOG.error("failed to set " + build + " destroyed");
                }
                break;
        }

        return savedBuild;
    }

    /**
     * Notify that the indicated task on the indicated build has failed.
     * @param build
     * @param task
     * @param error
     */
    public void notifyBuildTaskFailed(Build build, BuildTask task, String error) {
        buildService.setBuildStateToError(build, error);
    }

    /**
     * Notify that the indicated task on the indicated build has failed.
     * @param build
     * @param task
     * @param t
     */
    public void notifyBuildTaskFailed(Build build, BuildTask task, Throwable t) {
        Throwable cause = t;
        String reason = "";
        while (cause != null) {
            reason += (reason.length() == 0 ? "" : " ...caused by ")
                    + cause.getClass().getSimpleName()
                    + ": "
                    + cause.getMessage();
            cause = cause.getCause();
        }
        notifyBuildTaskFailed(build, task, reason);
    }

    /**
     * Mark the build as expired and start a destroy task.
     * @param build
     */
    private void expireBuild(Build build) {
        TestbedDriver testbedDriver;
        RouterDriver routerDriver;

        LOG.debug("loading drivers for " + build);
        try {
            Provider provider = build.getTestbed().getProvider();
            testbedDriver = driverService.loadTestbedDriver(provider.getTestbedDriver(), provider.getTestbedConfig());
            routerDriver = driverService.loadRouterDriver(provider.getRouterDriver(), provider.getRouterConfig());
        } catch (ReflectiveOperationException | ConfigurationException e) {
            buildService.notifyError(build, "failed to load drivers: " + e.getMessage());
            LOG.error("cannot load driver for " + build, e);
            return;
        }

        LOG.info("expiring " + build);
        try {
            buildService.setBuildState(build, BuildState.EXPIRED);
            executorService.execute(new DestroyBuildTask(build, testbedDriver, routerDriver, this, instanceService));
        } catch (InvalidStateChangeException e) {
            LOG.error("failed to expire " + build, e);
        }
    }

    /**
     * Mark the build as started and start a instantiate task.
     * @param build
     */
    private void startBuild(Build build) {
        TestbedDriver testbedDriver;
        RouterDriver routerDriver;

        LOG.debug("loading drivers for " + build);
        try {
            Provider provider = build.getTestbed().getProvider();
            testbedDriver = driverService.loadTestbedDriver(provider.getTestbedDriver(), provider.getTestbedConfig());
            routerDriver = driverService.loadRouterDriver(provider.getRouterDriver(), provider.getRouterConfig());
        } catch (ReflectiveOperationException | ConfigurationException e) {
            buildService.notifyError(build, "failed to load drivers: " + e.getMessage());
            LOG.error("cannot load driver for " + build, e);
            return;
        }

        LOG.info("starting " + build);
        try {
            buildService.setBuildState(build, BuildState.STARTED);
            executorService.execute(new InstantiateBuildTask(build, testbedDriver, routerDriver, this, instanceService));
        } catch (InvalidStateChangeException e) {
            LOG.error("failed to start " + build, e);
        }
    }

    /**
     * Start a provision task for the build.
     * @param build
     */
    private void provisionBuild(Build build) {
        executorService.execute(new ProvisionBuildTask(build, this, instanceService, driverService));
    }

    /**
     * Look for expired and scheduled builds and handle them appropriately.
     */
    @Scheduled(fixedRate = 10_000)
    private void findAndHandleBuilds() {
        // look for expired builds before scheduled builds, since it's more important that we
        // destroy expired builds than instantiating scheduled builds (consider cost of not
        // destroy a build)

        LOG.debug("looking for expired builds");
        Iterable<Build> expiredBuilds = buildService.findExpiredBuilds();
        for (Build build : expiredBuilds) {
            expireBuild(build);
        }

        LOG.debug("looking for scheduled builds");
        Iterable<Build> scheduledBuilds = buildService.findScheduledBuilds();
        for (Build build : scheduledBuilds) {
            startBuild(build);
        }
    }

}
