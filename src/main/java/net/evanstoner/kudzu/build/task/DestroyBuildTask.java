package net.evanstoner.kudzu.build.task;

import net.evanstoner.kudzu.build.BuildManager;
import net.evanstoner.kudzu.build.domain.*;
import net.evanstoner.kudzu.build.driver.RouterDriver;
import net.evanstoner.kudzu.build.driver.TestbedDriver;
import net.evanstoner.kudzu.build.exception.BuildException;
import net.evanstoner.kudzu.build.exception.InvalidTaskChangeException;
import net.evanstoner.kudzu.build.service.InstanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DestroyBuildTask extends AbstractBuildTask {

    private static final Logger LOG = LoggerFactory.getLogger(DestroyBuildTask.class);

    private InstanceService instanceService;
    private TestbedDriver testbedDriver;
    private RouterDriver routerDriver;

    public DestroyBuildTask(Build build, TestbedDriver testbedDriver, RouterDriver routerDriver,
                            BuildManager buildManager, InstanceService instanceService) {
        super(build, buildManager, BuildTask.DESTROYING);
        this.testbedDriver = testbedDriver;
        this.routerDriver = routerDriver;
        this.instanceService = instanceService;
    }

    @Override
    public void doTask() throws BuildException {
        LOG.info("destroying " + build);

        // the steps below are just the reverse of InstantiateBuildTask

        // 1. destroy access to the host instances
        destroyHostAccess();
        // 2. tear down the router since we're done messing with host access
        tearDownRouter();
        // 3. destroy the router entirely
        destroyRouter();

        // 4. destroy links
        destroyLinks();
        // 5. destroy hosts: links were the only dependent
        destroyHosts();
        // 6. destroy networks: links were the only dependent
        destroyNetworks();
        // 7. destroy testbed: components for isolation, or components shared across other resources
        destroyTestbed();
    }

    private void destroyTestbed() throws BuildException {
        TestbedInstance ti = build.getTestbedInstance();
        testbedDriver.destroyTestbedInstance(ti);
        instanceService.notifyInstanceDestroyed(ti);
    }

    private void destroyNetworks() throws BuildException {
        Iterable<NetworkInstance> networkInstances = build.getNetworkInstances();
        for (NetworkInstance ni : networkInstances) {
            testbedDriver.destroyNetworkInstance(ni);
            instanceService.notifyInstanceDestroyed(ni);
        }
    }

    private void destroyHosts() throws BuildException {
        Iterable<HostInstance> hostInstances = build.getHostInstances();
        for (HostInstance hi : hostInstances) {
            testbedDriver.destroyHostInstance(hi);
            instanceService.notifyInstanceDestroyed(hi);
        }
    }

    private void destroyLinks() throws BuildException {
        Iterable<LinkInstance> networkInstances = build.getLinkInstances();
        for (LinkInstance li : networkInstances) {
            testbedDriver.destroyLinkInstance(li);
            instanceService.notifyInstanceDestroyed(li);
        }
    }

    private void destroyRouter() throws BuildException {
        RouterInstance ri = build.getRouterInstance();
        testbedDriver.destroyRouterInstance(ri);
        instanceService.notifyInstanceDestroyed(ri);
    }

    private void tearDownRouter() throws BuildException {
        RouterInstance ri = build.getRouterInstance();
        routerDriver.deconfigureRouterInstance(ri);
        instanceService.routerInstanceService().notifyRouterInstanceDeconfigured(ri);
    }

    private void destroyHostAccess() {
        Iterable<HostInstance> hostInstances = build.getHostInstances();
        RouterInstance ri = build.getRouterInstance();
        for (HostInstance hi : hostInstances) {
            routerDriver.destroyHostInstanceAccess(hi, ri);
            instanceService.hostInstanceService().notifyHostInstanceInaccessible(hi);
        }
    }

}
