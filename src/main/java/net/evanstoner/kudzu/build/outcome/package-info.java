/**
 * Outcomes are simple data structures to support returning multiple values from a function call.
 */
package net.evanstoner.kudzu.build.outcome;