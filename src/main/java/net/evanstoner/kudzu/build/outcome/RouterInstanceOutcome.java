package net.evanstoner.kudzu.build.outcome;

public class RouterInstanceOutcome {

    public final String providerRef;
    public final String externalIpAddress;
    public final String accessDetails;

    public RouterInstanceOutcome(String providerRef, String externalIpAddress, String accessDetails) {
        this.providerRef = providerRef;
        this.externalIpAddress = externalIpAddress;
        this.accessDetails = accessDetails;
    }

}
