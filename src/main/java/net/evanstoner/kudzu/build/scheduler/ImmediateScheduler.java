package net.evanstoner.kudzu.build.scheduler;

import net.evanstoner.kudzu.testbed.domain.Testbed;

import java.util.Date;

public class ImmediateScheduler implements Scheduler {
    @Override
    public Date schedule(Testbed testbed) {
        return new Date();
    }
}
