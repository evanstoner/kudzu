package net.evanstoner.kudzu.build.scheduler;

import net.evanstoner.kudzu.testbed.domain.Testbed;

import java.util.Date;

public interface Scheduler {

    public Date schedule(Testbed testbed);

}
