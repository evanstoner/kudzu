package net.evanstoner.kudzu.build.domain;

public enum BuildState {
    /**
     * The build has been scheduled, but no worker has been selected to instantiate it. This is probably
     * because the startsOn date has not occurred yet.
     */
    SCHEDULED,

    /**
     * The build's instances are being built and provisioned.
     */
    STARTED,

    /**
     * All instances in the build have been built and provisioned.
     */
    ACTIVE,

    /**
     * The build is known to have expired but has not been destroyed yet.
     */
    EXPIRED,

    /**
     * All of the build's instances have been destroyed successfully.
     */
    DESTROYED,

    /**
     * Some error has occurred during any of the other states.
     */
    ERROR,

}
