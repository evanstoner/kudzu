package net.evanstoner.kudzu.build.domain;

public enum InstanceStatus {

    /**
     * The instance hasn't begun being built yet.
     */
    NONE,

    /**
     * The instance is being built.
     */
    IN_PROGRESS,

    /**
     * The instance has been built. It may or may not be "ready", i.e. "reachable" or "up".
     */
    ACTIVE,

    /**
     * The instance is being destroyed.
     */
    DESTROYING,

    /**
     * The instance has been destroyed.
     */
    DESTROYED,

    /**
     * Some error has occurred during any of the other states.
     */
    ERROR
}
