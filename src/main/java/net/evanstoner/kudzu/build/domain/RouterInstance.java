package net.evanstoner.kudzu.build.domain;

import com.fasterxml.jackson.annotation.JsonRootName;
import net.evanstoner.kudzu.testbed.domain.Router;

import javax.persistence.Entity;

@Entity
@JsonRootName("router_instance")
public class RouterInstance extends Instance<Router> {

    private String externalIpAddress;

    private String accessDetails;

    private Boolean configured = false;

    public RouterInstance() {
    }

    public RouterInstance(Router base, Build build) {
        super(base, build);
    }

    public String getExternalIpAddress() {
        return externalIpAddress;
    }

    public void setExternalIpAddress(String externalIpAddress) {
        this.externalIpAddress = externalIpAddress;
    }

    public String getAccessDetails() {
        return accessDetails;
    }

    public void setAccessDetails(String accessDetails) {
        this.accessDetails = accessDetails;
    }

    public Boolean isConfigured() {
        return configured;
    }

    public void setConfigured(Boolean configured) {
        this.configured = configured;
    }
}
