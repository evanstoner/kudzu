package net.evanstoner.kudzu.build.domain;

import com.fasterxml.jackson.annotation.JsonRootName;
import net.evanstoner.kudzu.testbed.domain.Host;

import javax.persistence.Entity;

@Entity
@JsonRootName("hostInstance")
public class HostInstance extends Instance<Host> {

    private String accessIpAddress;

    private Integer accessPort;

    private String accessError;

    public HostInstance() {
        super();
    }

    public HostInstance(Host host, Build build) {
        super(host, build);
    }

    public String getAccessIpAddress() {
        return accessIpAddress;
    }

    public void setAccessIpAddress(String accessIpAddress) {
        this.accessIpAddress = accessIpAddress;
    }

    public Integer getAccessPort() {
        return accessPort;
    }

    public void setAccessPort(Integer accessPort) {
        this.accessPort = accessPort;
    }

    public String getAccessError() {
        return accessError;
    }

    public void setAccessError(String accessError) {
        this.accessError = accessError;
    }
}
