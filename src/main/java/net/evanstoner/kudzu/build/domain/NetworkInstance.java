package net.evanstoner.kudzu.build.domain;

import com.fasterxml.jackson.annotation.JsonRootName;
import net.evanstoner.kudzu.testbed.domain.Network;

import javax.persistence.Entity;

@Entity
@JsonRootName("networkInstance")
public class NetworkInstance extends Instance<Network> {

    public NetworkInstance() {
        super();
    }

    public NetworkInstance(Network base, Build build) {
        super(base, build);
    }

}
