package net.evanstoner.kudzu.build.domain;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import net.evanstoner.spring.entity.BaseEntity;
import net.evanstoner.kudzu.testbed.domain.TestbedEntity;
import org.springframework.util.Assert;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * @param <D> The type this class is an instance of.
 */

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Instance<D extends TestbedEntity> extends BaseEntity {

    private Date createdOn;

    private Date destroyedOn;

    @Enumerated(EnumType.STRING)
    private InstanceStatus status = InstanceStatus.NONE;

    private String error;

    private String providerRef;

    private String providerData;

    @ManyToOne
    @JoinColumn(name = "build", nullable = false, updatable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Build build = null;

    // if we don't specify the targetEntity as the superclass of D, Hibernate will choose one of the
    // subclasses (e.g. Network or Testbed) as the foreign key and severely screw things up
    @ManyToOne(targetEntity = TestbedEntity.class)
    @JoinColumn(name = "base", nullable = false, updatable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private D base = null;

    public Instance() {}

    public Instance(D base, Build build) {
        this.base = base;
        this.build = build;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getDestroyedOn() {
        return destroyedOn;
    }

    public void setDestroyedOn(Date destroyedOn) {
        this.destroyedOn = destroyedOn;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public InstanceStatus getStatus() {
        return status;
    }

    public void setStatus(InstanceStatus status) {
        this.status = status;
    }

    public String getProviderRef() {
        return providerRef;
    }

    public void setProviderRef(String providerRef) {
        this.providerRef = providerRef;
    }

    public String getProviderData() {
        return providerData;
    }

    public void setProviderData(String providerData) {
        this.providerData = providerData;
    }

    public Build getBuild() {
        return build;
    }

    public void setBuild(Build build) {
        Assert.isNull(build, "instance's build must be null in order to set");
        this.build = build;
    }

    public D getBase() {
        return base;
    }

    public void setBase(D base) {
        Assert.isNull(base, "instance's base must be null in order to set");
        this.base = base;
    }

    public String toString() {
        return super.toString() + "[" + getProviderRef() + "]";
    }

}
