package net.evanstoner.kudzu.build.domain;

import com.fasterxml.jackson.annotation.*;
import net.evanstoner.spring.entity.BaseEntity;
import net.evanstoner.kudzu.testbed.domain.Testbed;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@JsonRootName("build")
public class Build extends BaseEntity {

    private String name;

    private BuildState state;

    private BuildTask task;

    private Date submittedOn;

    private Date startsOn;

    private Date expiresOn;

    private Date destroyedOn;

    private String error;

    @ManyToOne
    @JoinColumn(name = "testbed", updatable = false, nullable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Testbed testbed;

    // we cannot have @OneToMany references to each type of instance, e.g. networkInstances, because
    // relationship lookups cannot be performed on members of superclasses; we can however load the
    // relationships as superclasses and filter them in our own getter methods; load this relationship
    // eagerly since we know the Build will likely be passed around outside of the hibernate session
    @OneToMany(mappedBy = "build", fetch = FetchType.EAGER)
    @JsonIdentityReference(alwaysAsId = true)
    private Collection<Instance> instances;

    Build() {}

    public Build(Long id) {
        this.id = id;
    }

    public Build(Testbed testbed) {
        this.testbed = testbed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BuildState getState() {
        return state;
    }

    public void setState(BuildState state) {
        this.state = state;
    }

    public BuildTask getTask() {
        return task;
    }

    public void setTask(BuildTask task) {
        this.task = task;
    }

    public Date getSubmittedOn() {
        return submittedOn;
    }

    public void setSubmittedOn(Date submittedOn) {
        this.submittedOn = submittedOn;
    }

    public Date getStartsOn() {
        return startsOn;
    }

    public void setStartsOn(Date startsOn) {
        this.startsOn = startsOn;
    }

    public Date getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(Date expiresOn) {
        this.expiresOn = expiresOn;
    }

    public Date getDestroyedOn() {
        return destroyedOn;
    }

    public void setDestroyedOn(Date destroyedOn) {
        this.destroyedOn = destroyedOn;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public void setTestbed(Testbed testbed) {
        this.testbed = testbed;
    }

    @JsonProperty("testbed")
    public void setTestbed(Long testbedId) {
        this.testbed = new Testbed(testbedId);
    }

    @JsonIgnore
    public Collection<Instance> getInstances() {
        return instances;
    }

    public void setInstances(Collection<Instance> instances) {
        this.instances = instances;
    }

    @JsonIdentityReference(alwaysAsId = true)
    public TestbedInstance getTestbedInstance() {
        if (this.instances == null) return null;

        return instances.stream()
                .filter(i -> i instanceof TestbedInstance)
                .map(i -> (TestbedInstance)i)
                .findFirst().get();
    }

    @JsonIdentityReference(alwaysAsId = true)
    public RouterInstance getRouterInstance() {
        if (this.instances == null) return null;

        return instances.stream()
                .filter(i -> i instanceof RouterInstance)
                .map(i -> (RouterInstance)i)
                .findFirst().get();
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Collection<NetworkInstance> getNetworkInstances() {
        if (this.instances == null) return null;

        return this.instances.stream()
                .filter(i -> i instanceof NetworkInstance)
                .map(i -> (NetworkInstance)i)
                .collect(Collectors.toList());
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Collection<HostInstance> getHostInstances() {
        if (this.instances == null) return null;

        return this.instances.stream()
                .filter(i -> i instanceof HostInstance)
                .map(i -> (HostInstance)i)
                .collect(Collectors.toList());
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Collection<LinkInstance> getLinkInstances() {
        if (this.instances == null) return null;

        return this.instances.stream()
                .filter(i -> i instanceof LinkInstance)
                .map(i -> (LinkInstance)i)
                .collect(Collectors.toList());
    }

    @JsonIdentityReference(alwaysAsId = true)
    public Collection<PersonaAssignmentInstance> getPersonaAssignmentInstances() {
        if (this.instances == null) return null;

        return this.instances.stream()
                .filter(i -> i instanceof PersonaAssignmentInstance)
                .map(i -> (PersonaAssignmentInstance)i)
                .collect(Collectors.toList());
    }


}
