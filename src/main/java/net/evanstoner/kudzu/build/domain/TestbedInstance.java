package net.evanstoner.kudzu.build.domain;

import com.fasterxml.jackson.annotation.JsonRootName;
import net.evanstoner.kudzu.testbed.domain.Testbed;

import javax.persistence.Entity;

@Entity
@JsonRootName("testbedInstance")
public class TestbedInstance extends Instance<Testbed> {

    public TestbedInstance() {
        super();
    }

    public TestbedInstance(Testbed base, Build build) {
        super(base, build);
    }

}

