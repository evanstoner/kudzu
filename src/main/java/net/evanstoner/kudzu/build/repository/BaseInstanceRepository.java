package net.evanstoner.kudzu.build.repository;

import net.evanstoner.kudzu.build.domain.Build;
import net.evanstoner.kudzu.build.domain.Instance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface BaseInstanceRepository<T extends Instance> extends CrudRepository<T, Long> {

    public Iterable<T> findByBuild(Build build);

}
