package net.evanstoner.kudzu.build.repository;

import net.evanstoner.kudzu.build.domain.HostInstance;

public interface HostInstanceRepository extends BaseInstanceRepository<HostInstance> {
}
