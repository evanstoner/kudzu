package net.evanstoner.kudzu.build.repository;

import net.evanstoner.kudzu.build.domain.PersonaAssignmentInstance;

public interface PersonaAssignmentInstanceRepository extends BaseInstanceRepository<PersonaAssignmentInstance> {
}
