package net.evanstoner.kudzu.build.repository;

import net.evanstoner.kudzu.build.domain.Build;
import net.evanstoner.kudzu.build.domain.BuildState;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Date;

public interface BuildRepository extends CrudRepository<Build, Long> {

    public Iterable<Build> findByTestbed(Testbed testbed);

    public Iterable<Build> findByStateAndStartsOnLessThan(BuildState state, Date startOn);

    public Iterable<Build> findByStateAndExpiresOnLessThan(BuildState state, Date expiresOn);

    public Iterable<Build> findByStateInAndExpiresOnLessThan(Collection<BuildState> states, Date expiresOn);

}
