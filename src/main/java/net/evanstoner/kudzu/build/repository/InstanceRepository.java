package net.evanstoner.kudzu.build.repository;

import net.evanstoner.kudzu.build.domain.Instance;

public interface InstanceRepository extends BaseInstanceRepository<Instance> {
}
