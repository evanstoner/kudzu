package net.evanstoner.kudzu.build.repository;

import net.evanstoner.kudzu.build.domain.NetworkInstance;

public interface NetworkInstanceRepository extends BaseInstanceRepository<NetworkInstance> {
}
