package net.evanstoner.kudzu.build.driver;

import net.evanstoner.kudzu.build.domain.*;
import net.evanstoner.kudzu.build.exception.BuildException;
import net.evanstoner.kudzu.build.service.BuildService;
import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.testbed.domain.*;

public abstract class TestbedDriver {

    protected BuildService buildService;

    public TestbedDriver(BuildService buildService) {
        this.buildService = buildService;
    }

    abstract public void configure(String config) throws ConfigurationException;

    abstract public String buildTestbed(Testbed testbed) throws BuildException;

    abstract public void destroyTestbedInstance(TestbedInstance testbedInstance) throws BuildException;

    abstract public String buildNetwork(Network network) throws BuildException;

    abstract public void destroyNetworkInstance(NetworkInstance networkInstance) throws BuildException;

    abstract public String buildHost(Host host) throws BuildException;

    abstract public void destroyHostInstance(HostInstance hostInstance) throws BuildException;

    //TODO: buildHostAndLinks(Host host, Collection<Link> links)

    //TODO: destroyHostAndLinkInstances(HostInstance hostInstance, Collection<LinkInstance> linkInstances)

    abstract public String buildLink(Link link) throws BuildException;

    abstract public void destroyLinkInstance(LinkInstance linkInstance) throws BuildException;

    abstract public String buildRouter(Router router, TestbedInstance testbedInstance) throws BuildException;

    abstract public void destroyRouterInstance(RouterInstance routerInstance) throws BuildException;

}
