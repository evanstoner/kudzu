package net.evanstoner.kudzu.build.driver;

import net.evanstoner.kudzu.build.domain.HostInstance;
import net.evanstoner.kudzu.build.domain.RouterInstance;
import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.testbed.domain.RouterType;

public abstract class RouterDriver {

    public abstract RouterType getType();

    public abstract void configure(String config) throws ConfigurationException;

    /**
     *
     * @param routerInstance
     * @return A String array of length 2, where the first element is externalIpAddress and the
     * second is accessDetails. Both elements may be null.
     */
    public abstract String[] configureRouterInstance(RouterInstance routerInstance);

    public abstract void deconfigureRouterInstance(RouterInstance routerInstance);

    /**
     *
     * @param hostInstance
     * @param routerInstance
     * @return An array of length 2, where the first element is a String accessIpAddress and the
     * second is an Integer accessPort. Both may be null.
     */
    public abstract Object[] createHostInstanceAccess(HostInstance hostInstance, RouterInstance routerInstance);

    public abstract void destroyHostInstanceAccess(HostInstance hostInstance, RouterInstance routerInstance);

}
