package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.Instance;
import net.evanstoner.kudzu.build.domain.InstanceStatus;
import net.evanstoner.kudzu.build.repository.InstanceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Transactional
@Service
public class InstanceServiceImpl implements InstanceService
{

    private static final Logger LOG = LoggerFactory.getLogger(InstanceServiceImpl.class);

    @Autowired
    private HostInstanceService hostInstanceService;
    @Autowired
    private RouterInstanceService routerInstanceService;
    @Autowired
    private PersonaAssignmentInstanceService personaAssignmentInstanceService;

    @Autowired
    private InstanceRepository instanceRepository;

    @Override
    public void notifyInstanceActive(Instance instance, String providerRef) {
        notifyInstanceActive(instance, providerRef, null);
    }

    @Override
    public void notifyInstanceActive(Instance instance, String providerRef, String providerData) {
        instance.setStatus(InstanceStatus.ACTIVE);
        instance.setCreatedOn(new Date());
        instance.setProviderRef(providerRef);
        instance.setProviderData(providerData);

        LOG.info(instance + " is active; provider data: " + instance.getProviderData());

        instanceRepository.save(instance);
    }

    @Override
    public void notifyInstanceDestroyed(Instance instance) {
        instance.setStatus(InstanceStatus.DESTROYED);
        instance.setDestroyedOn(new Date());

        LOG.info(instance + " is destroyed");

        instanceRepository.save(instance);
    }

    @Override
    public void notifyInstanceError(Instance instance, String error) {
        notifyInstanceError(instance, error, null);
    }

    @Override
    public void notifyInstanceError(Instance instance, String error, String providerRef) {
        instance.setStatus(InstanceStatus.ERROR);
        instance.setCreatedOn(new Date());
        instance.setProviderRef(providerRef);
        instance.setError(error);

        LOG.warn(instance + " has erred: " + instance.getError());

        instanceRepository.save(instance);
    }

    @Override
    public HostInstanceService hostInstanceService() {
        return hostInstanceService;
    }

    @Override
    public RouterInstanceService routerInstanceService() {
        return routerInstanceService;
    }

    @Override
    public PersonaAssignmentInstanceService personaAssignmentInstanceService() {
        return personaAssignmentInstanceService;
    }

}
