package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.HostInstance;

public interface HostInstanceService extends BaseInstanceService<HostInstance> {

    void notifyHostInstanceAccessible(HostInstance hostInstance);

    void notifyHostInstanceAccessible(HostInstance hostInstance, String accessIpAddress);

    void notifyHostInstanceAccessible(HostInstance hostInstance, String accessIpAddress, Integer accessPort);

    void notifyHostInstanceAccessError(HostInstance hostInstance, String error);

    void notifyHostInstanceInaccessible(HostInstance hostInstance);

}
