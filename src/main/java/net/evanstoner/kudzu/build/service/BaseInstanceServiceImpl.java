package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.Build;
import net.evanstoner.kudzu.build.domain.Instance;
import net.evanstoner.kudzu.build.repository.BaseInstanceRepository;
import net.evanstoner.spring.service.GenericReadOnlyServiceImpl;

public class BaseInstanceServiceImpl<D extends Instance>
        extends GenericReadOnlyServiceImpl<D, BaseInstanceRepository<D>>
        implements BaseInstanceService<D> {
    @Override
    public Iterable<D> findByBuildId(Long buildId) {
        return this.repository.findByBuild(new Build(buildId));
    }
}
