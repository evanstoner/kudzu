package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.BuildManager;
import net.evanstoner.kudzu.build.domain.*;
import net.evanstoner.kudzu.build.driver.RouterDriver;
import net.evanstoner.kudzu.build.driver.TestbedDriver;
import net.evanstoner.kudzu.build.exception.InvalidStateChangeException;
import net.evanstoner.kudzu.build.exception.InvalidTaskChangeException;
import net.evanstoner.kudzu.build.repository.*;
import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import net.evanstoner.kudzu.provision.domain.PersonaAssignment;
import net.evanstoner.kudzu.provision.driver.ProvisionDriver;
import net.evanstoner.kudzu.provision.repository.PersonaAssignmentRepository;
import net.evanstoner.kudzu.testbed.domain.Host;
import net.evanstoner.kudzu.testbed.domain.Link;
import net.evanstoner.kudzu.testbed.domain.Network;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import net.evanstoner.kudzu.testbed.repository.HostRepository;
import net.evanstoner.kudzu.testbed.repository.LinkRepository;
import net.evanstoner.kudzu.testbed.repository.NetworkRepository;
import net.evanstoner.kudzu.testbed.repository.TestbedRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.Null;
import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.Date;

@Transactional
@Service
public class BuildServiceImpl
        extends GenericReadWriteServiceImpl<Build, BuildRepository>
        implements BuildService {

    private static final Logger LOG = LoggerFactory.getLogger(BuildServiceImpl.class);

    @Autowired
    BuildManager buildManager;

    @Autowired
    TestbedRepository testbedRepo;
    @Autowired
    NetworkRepository networkRepo;
    @Autowired
    HostRepository hostRepo;
    @Autowired
    LinkRepository linkRepo;

    // InstanceRepository is used to perform generic operations on instances
    @Autowired
    InstanceRepository instanceRepo;

    @Autowired
    PersonaAssignmentRepository personaAssignmentRepo;

    /**
     * Create a Build and its Instances, and submit it to the BuildManager.
     * @param build
     * @return
     */
    @Override
    public Build create(Build build) {
        Build savedBuild = save(build);
        Testbed testbed = testbedRepo.findOne(build.getTestbed().getId());

        // create the testbed instance
        TestbedInstance testbedInstance = new TestbedInstance(testbed, build);
        instanceRepo.save(testbedInstance);

        // create the network instances
        Iterable<Network> networks = testbed.getNetworks();
        for (Network network : networks) {
            NetworkInstance networkInstance = new NetworkInstance(network, build);
            instanceRepo.save(networkInstance);
        }

        // create the host instances
        Iterable<Host> hosts = testbed.getHosts();
        for (Host host : hosts) {
            HostInstance hostInstance = new HostInstance(host, build);
            instanceRepo.save(hostInstance);
        }

        // create the link instances
        Iterable<Link> links = testbed.getLinks();
        for (Link link : links) {
            LinkInstance linkInstance = new LinkInstance(link, build);
            instanceRepo.save(linkInstance);
        }

        // create the router instance
        RouterInstance routerInstance = new RouterInstance(testbed.getRouter(), build);
        instanceRepo.save(routerInstance);

        // create the persona assignment instances
        Iterable<PersonaAssignment> personaAssignments = personaAssignmentRepo.findByHostTestbed(testbed);
        for (PersonaAssignment pa : personaAssignments) {
            PersonaAssignmentInstance paInstance = new PersonaAssignmentInstance(pa, build);
            instanceRepo.save(paInstance);
        }

        // submitting the build sets the submittedOn and startsOn dates
        buildManager.submit(savedBuild);

        return update(savedBuild);
    }

    @Override
    public Iterable<Build> findByTestbedId(Long testbedId) {
        return this.repository.findByTestbed(new Testbed(testbedId));
    }

    @Override
    public Iterable<Build> findScheduledBuilds() {
        return repository.findByStateAndStartsOnLessThan(BuildState.SCHEDULED, new Date());
    }

    @Override
    public Iterable<Build> findExpiredBuilds() {
        return repository.findByStateInAndExpiresOnLessThan(
                Arrays.asList(BuildState.ACTIVE, BuildState.ERROR),
                new Date());
    }

    @Override
    public Build notifyActive(Build build) throws InvalidStateChangeException {
        return setBuildState(build, BuildState.ACTIVE);
    }

    @Override
    public Build notifyDestroyed(Build build) throws InvalidStateChangeException {
        build.setDestroyedOn(new Date());
        Build savedBuild = save(build);
        return setBuildState(savedBuild, BuildState.DESTROYED);
    }

    @Override
    public Build notifyError(Build build, String error) {
        return setBuildStateToError(build, error);
    }

    @Override
    public Build setBuildState(Build build, BuildState state) throws InvalidStateChangeException {
        if (state == null) {
            throw new IllegalArgumentException("state cannot be null");
        }

        if (state == BuildState.ERROR) {
            return setBuildStateToError(build, null);
        } else {
            LOG.info(build + " transitioning from " + build.getState() + " to " + state);
            build.setState(state);
            build.setTask(null);
            if (state == BuildState.DESTROYED) {
                build.setDestroyedOn(new Date());
            }
            return update(build);
        }
    }

    @Override
    public Build setBuildStateToError(Build build, String error) {
        if (error == null || error.trim().length() == 0) {
            LOG.warn(build + " transitioning to error state without error description");
        }
        LOG.warn(build + " transitioning from " + build.getState() + " to " + BuildState.ERROR + ": " + error);
        build.setState(BuildState.ERROR);
        build.setError(error);
        build.setTask(null);
        return update(build);
    }

    @Override
    public Build setBuildTask(Build build, BuildTask task) throws InvalidTaskChangeException {
        // rule: the current task is set XOR the new task is set
        if (build.getTask() != null && task != null) {
            throw new InvalidTaskChangeException(task, "already in task " + task);
        }

        LOG.info(build + " task changing to " + task);

        build.setTask(task);
        return update(build);
    }

}
