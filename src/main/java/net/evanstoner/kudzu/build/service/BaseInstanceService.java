package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.Instance;
import net.evanstoner.spring.service.GenericReadOnlyService;

/**
 * A light wrapper around {@code GenericReadOnlyService} with some Instance/Build-specific methods.
 * @param <D>
 */
public interface BaseInstanceService<D extends Instance> extends GenericReadOnlyService<D> {

    Iterable<D> findByBuildId(Long buildId);

}
