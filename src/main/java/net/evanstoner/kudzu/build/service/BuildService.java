package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.*;
import net.evanstoner.kudzu.build.driver.RouterDriver;
import net.evanstoner.kudzu.build.driver.TestbedDriver;
import net.evanstoner.kudzu.build.exception.InvalidStateChangeException;
import net.evanstoner.kudzu.build.exception.InvalidTaskChangeException;
import net.evanstoner.spring.service.GenericReadWriteService;
import net.evanstoner.kudzu.provision.driver.ProvisionDriver;

public interface BuildService extends GenericReadWriteService<Build> {

    Iterable<Build> findByTestbedId(Long testbedId);

    Iterable<Build> findScheduledBuilds();

    Iterable<Build> findExpiredBuilds();

    Build notifyActive(Build build) throws InvalidStateChangeException;

    Build notifyDestroyed(Build build) throws InvalidStateChangeException;

    Build notifyError(Build build, String error);

    Build setBuildState(Build build, BuildState state) throws InvalidStateChangeException;

    Build setBuildStateToError(Build build, String error);

    Build setBuildTask(Build build, BuildTask task) throws InvalidTaskChangeException;

}
