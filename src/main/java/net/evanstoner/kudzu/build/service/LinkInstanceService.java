package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.LinkInstance;

public interface LinkInstanceService extends BaseInstanceService<LinkInstance> {
}
