package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.Build;
import net.evanstoner.kudzu.build.domain.HostInstance;
import net.evanstoner.kudzu.build.repository.HostInstanceRepository;
import net.evanstoner.kudzu.build.repository.InstanceRepository;
import net.evanstoner.spring.service.GenericReadOnlyServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class HostInstanceServiceImpl
        extends BaseInstanceServiceImpl<HostInstance>
        implements HostInstanceService {

    private static final Logger LOG = LoggerFactory.getLogger(HostInstanceServiceImpl.class);

    @Override
    public Iterable<HostInstance> findByBuildId(Long buildId) {
        return this.repository.findByBuild(new Build(buildId));
    }

    @Override
    public void notifyHostInstanceAccessible(HostInstance hostInstance) {
        notifyHostInstanceAccessible(hostInstance, null, null);
    }

    @Override
    public void notifyHostInstanceAccessible(HostInstance hostInstance, String accessIpAddress) {
        notifyHostInstanceAccessible(hostInstance, accessIpAddress, null);
    }

    @Override
    public void notifyHostInstanceAccessible(HostInstance hostInstance, String accessIpAddress, Integer accessPort) {
        hostInstance.setAccessIpAddress(accessIpAddress);
        hostInstance.setAccessPort(accessPort);

        LOG.info(String.format("%s is accessible: %s:%s",
                hostInstance,
                hostInstance.getAccessIpAddress(),
                hostInstance.getAccessPort()));

        repository.save(hostInstance);
    }

    @Override
    public void notifyHostInstanceAccessError(HostInstance hostInstance, String error) {
        hostInstance.setAccessError(error);

        LOG.warn(String.format("%s is inaccessible due to error: %s",
                hostInstance,
                hostInstance.getAccessError()));

        repository.save(hostInstance);
    }

    @Override
    public void notifyHostInstanceInaccessible(HostInstance hostInstance) {
        //TODO: should we actually set these to null? would it be useful to have the history?
        hostInstance.setAccessIpAddress(null);
        hostInstance.setAccessPort(null);

        LOG.info(hostInstance + " is no longer accessible");

        repository.save(hostInstance);
    }

}
