package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.InstanceStatus;
import net.evanstoner.kudzu.build.domain.PersonaAssignmentInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

@Transactional
@Service
public class PersonaAssignmentInstanceServiceImpl
        extends BaseInstanceServiceImpl<PersonaAssignmentInstance>
        implements PersonaAssignmentInstanceService {

    private static final Logger LOG = LoggerFactory.getLogger(PersonaAssignmentInstanceServiceImpl.class);

    @Override
    public void notifyApplied(PersonaAssignmentInstance pai, String result) {
        LOG.info(pai + " was applied successfully");

        pai.setResult(result);
        pai.setCreatedOn(new Date());
        pai.setStatus(InstanceStatus.ACTIVE);

        repository.save(pai);
    }

}
