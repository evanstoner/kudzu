package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.Instance;

public interface InstanceService {

    void notifyInstanceActive(Instance instance, String providerRef);

    void notifyInstanceActive(Instance instance, String providerRef, String providerData);

    void notifyInstanceDestroyed(Instance instance);

    void notifyInstanceError(Instance instance, String error);

    void notifyInstanceError(Instance instance, String error, String providerRef);

    HostInstanceService hostInstanceService();

    RouterInstanceService routerInstanceService();

    PersonaAssignmentInstanceService personaAssignmentInstanceService();

}
