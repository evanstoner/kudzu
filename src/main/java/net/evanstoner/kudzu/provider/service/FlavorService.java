package net.evanstoner.kudzu.provider.service;

import net.evanstoner.spring.service.GenericReadWriteService;
import net.evanstoner.kudzu.provider.domain.Flavor;

public interface FlavorService extends GenericReadWriteService<Flavor> {

    public Iterable<Flavor> findByProviderId(long providerId);

}
