package net.evanstoner.kudzu.provider.service;

import net.evanstoner.spring.service.GenericReadWriteService;
import net.evanstoner.kudzu.provider.domain.Image;

public interface ImageService extends GenericReadWriteService<Image> {

    public Iterable<Image> findByProviderId(long providerId);

}
