package net.evanstoner.kudzu.provider.service;

import net.evanstoner.kudzu.provider.domain.Provider;
import net.evanstoner.kudzu.provider.repository.ProviderRepository;
import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ProviderServiceImpl extends GenericReadWriteServiceImpl<Provider, ProviderRepository> implements ProviderService {

}
