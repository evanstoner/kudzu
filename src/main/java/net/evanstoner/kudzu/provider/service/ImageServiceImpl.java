package net.evanstoner.kudzu.provider.service;

import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import net.evanstoner.kudzu.provider.domain.Image;
import net.evanstoner.kudzu.provider.domain.Provider;
import net.evanstoner.kudzu.provider.repository.ImageRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ImageServiceImpl extends GenericReadWriteServiceImpl<Image, ImageRepository> implements ImageService {

    @Override
    public Iterable<Image> findByProviderId(long providerId) {
        return repository.findByProvider(new Provider(providerId));
    }

}
