package net.evanstoner.kudzu.provider.repository;

import net.evanstoner.kudzu.provider.domain.Image;
import net.evanstoner.kudzu.provider.domain.Provider;
import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<Image, Long> {

    Iterable<Image> findByProvider(Provider provider);

}
