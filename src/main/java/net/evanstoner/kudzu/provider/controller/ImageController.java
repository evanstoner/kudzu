package net.evanstoner.kudzu.provider.controller;

import net.evanstoner.spring.controller.GenericReadOnlyController;
import net.evanstoner.kudzu.provider.domain.Image;
import net.evanstoner.kudzu.provider.service.ImageService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/images")
public class ImageController extends GenericReadOnlyController<Image, ImageService> {

    @RequestMapping(method = RequestMethod.GET, params = "provider")
    public Map getByProviderId(@RequestParam("provider") Long providerId) {
        return wrapMany(this.service.findByProviderId(providerId));
    }

}
