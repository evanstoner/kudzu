package net.evanstoner.kudzu.provider.controller;

import net.evanstoner.spring.controller.GenericReadOnlyController;
import net.evanstoner.kudzu.provider.domain.Flavor;
import net.evanstoner.kudzu.provider.service.FlavorService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/flavors")
public class FlavorController extends GenericReadOnlyController<Flavor, FlavorService> {

    @RequestMapping(method = RequestMethod.GET, params = "provider")
    public Map getByTestbedId(@RequestParam("provider") Long providerId) {
        return wrapMany(this.service.findByProviderId(providerId));
    }

}
