package net.evanstoner.kudzu.provider.controller;

import net.evanstoner.spring.controller.GenericReadWriteController;
import net.evanstoner.kudzu.provider.domain.Provider;
import net.evanstoner.kudzu.provider.service.ProviderService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/providers")
public class ProviderController extends GenericReadWriteController<Provider, ProviderService> {
}
