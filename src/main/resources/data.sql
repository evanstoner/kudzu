insert into project(id, name, description) values
  (101, 'default', 'The default project.');

insert into provider(id, name, testbed_driver, testbed_config, router_driver, router_config) values
  (151, 'No Op Provider',
      'net.evanstoner.kudzu.driver.testbed.NoopTestbedDriver',
      NULL,
      'net.evanstoner.kudzu.driver.router.NoopRouterDriver',
      NULL),
  (152, 'File Provider',
      'net.evanstoner.kudzu.driver.testbed.FileTestbedDriver',
      '/tmp/',
      'net.evanstoner.kudzu.driver.router.NoopRouterDriver',
      NULL);

insert into image(id, name, username, password, provider_ref, provider) values
  (201, 'ubuntu-trusty-cloudimg', 'ubuntu', null, '51515b2b-5e68-4f65-9c16-68fb32c67790', 151),
  (202, 'ubuntu-precise-cloudimg', 'ubuntu', null, 'a77b6b97-66fc-4a90-8cb5-d4507f4e3a94', 151),
  (203, 'cirros-0.3.4', 'cirros', 'cubswin:)', 'eea429dc-f766-4d08-ac78-7967d03b4e91', 151),
  (204, 'file-image', null, null, 'file-image', 152);

insert into flavor(id, name, provider_ref, provider) values
  (251, 'default', 'default', 151),
  (252, 'file-flavor', 'file-flavor', 152);

insert into testbed(id, name, description, project, provider) values
  (301, 'Hello world', 'The default testbed.', 101, 151),
  (302, 'Filebed', 'Testing the file drivers.', 101, 152);

insert into router(id, type, testbed) values
  (351, 'FLAT', 301),
  (352, 'FLAT', 302);

insert into host(id, name, image, flavor, testbed) values
  (401, 'foo', 201, 251, 301),
  (402, 'bar', 203, 251, 301),
  (403, 'host1', 204, 252, 302),
  (404, 'host2', 204, 252, 302);

insert into network(id, name, cidr, service, testbed) values
  (451, 'service', '10.0.1.0/24', true, 301),
  (452, 'demo', '192.168.1.0/24', false, 301),
  (453, 'service', '10.0.2.0/24', true, 302),
  (454, 'network1', '172.16.3.0/24', false, 302);

insert into link(id, ip_address, host, network, testbed) values
  (501, '10.0.1.10', 401, 451, 301),
  (502, '10.0.1.11', 402, 451, 301),
  (503, '192.168.1.101', 401, 452, 301),
  (504, '192.168.1.102', 402, 452, 301),
  (505, '10.0.2.10', 403, 453, 302),
  (506, '10.0.2.11', 404, 453, 302),
  (507, '172.16.3.10', 403, 454, 302),
  (508, '172.16.3.11', 404, 454, 302);

insert into provisioner(id, name, provision_driver, provision_config) values
  (601, 'No Op Provisioner', 'net.evanstoner.kudzu.driver.provision.NoopProvisionDriver', '{\n"foo": "bar"\n}');

insert into persona(id, name, source, source_driver, default_config, provisioner) values
  (651, 'lorem', '/home/kudzu/personas/lorem', 'net.evanstoner.kudzu.driver.source.FilesystemSourceDriver', '[section]\nkey = value', 601),
  (652, 'ipsum', '/home/kudzu/personas/ipsum', 'net.evanstoner.kudzu.driver.source.FilesystemSourceDriver', '[section]\nkey = value', 601);

insert into persona_assignment(id, host, persona, priority, config) values
  (701, 401, 651, 50, '[section]\nkey = value'),
  (702, 401, 652, 20, '[section]\nkey = value'),
  (703, 402, 651, 50, '[section]\nkey = value'),
  (704, 402, 652, 80, '[section]\nkey = value');

-- ensure new entities don't collide with our bootstrapped values
alter sequence hibernate_sequence restart with 100;
